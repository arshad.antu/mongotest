<?php 
	namespace Models\Queries;

	//Import the MongoDB PHP Library
	use MongoDB;

	class Queries {	
		//Connect to MongoDB. Creates a MongoDB Client
		function createMongoClient () {
			$con = new MongoDB\Client("mongodb://localhost:27017");
			if ($con) {
				echo "Created a MongoDB Client object successfully.<br>";
				return $con;
			} else {
				throw new MongoDB\Driver\Exception\Exception;
			}	
		}

		//Show Databases in MongoDB. Only prints the databases available in the connected Mongo Server
		function showDB ($con) {
			if ($con) {
				try {
					$databases = $con->listDatabases();
					echo "Databases in $con:<br>";
					foreach ($databases as $result) {
						echo $result->getName()."<br>";
					}	
				} catch (MongoDB\Driver\Exception\ConnectionTimeoutException $cte) {
					echo "Connecting to database failed. Here is some error dump:<br>".$cte->getMessage();
				}		 
			} else {
				echo "No MongoDB connection/client object found.<br>";
			}
		}

		//Selects a Database from MongoDB. Returns a MongoDB/Database object.
		function selectDB ($con, $dbName) {
			if ($con && is_string($dbName)) {
				try {
					$database = $con->selectDatabase($dbName);
					echo "Database: $database is selected.<br>";	
					return $database;
				} catch (MongoDB\Exception\InvalidArgumentException $iae) {
					echo "Selecting database failed. Here is some error dump:<br>".$iae->getMessage();
				}		 
			} else {
				echo "No Database Specified or No MongoDB connection/client object found.<br>";
			}
		}
	}

?>