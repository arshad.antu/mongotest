<?php 
	//Require the Autoloader for the entire project
	require "vendor/autoload.php";

	//For now, this Application Controller instantiates a loginQuery. So instantiating this Conts\Application will start the query.
	new Conts\Application;
?>